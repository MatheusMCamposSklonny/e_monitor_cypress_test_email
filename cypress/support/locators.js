const locators = {

    LOGIN: {
        USER: '#Email',
        PASSWORD: '#Senha',
        BTN_LOGIN: '#lnkLogin'
    },

    MODULES: {
        EAUDITOR: '.eauditor > .flipper > .front > .btn',
        ESIMULADOR: '.esimulador > .flipper > .front > .btn',
        EDRIVE: '.edrive > .flipper > .front > .btn',
        ECONSULTA: '.econsulta > .flipper > .front > .btn',
        ERECUPERADOR: '.erecuperador > .flipper > .front > .btn',
        EMONITOR: '.emonitor > .flipper > .front > .btn',
        ECAPTURADOR: '.e360 > .flipper > .front > .btn',
        EATENDIMENTO: '.eatendimento > .flipper > .front > .btn',
    },

    EMULTIPLICADOR: {
        BODY: {
            TOPO: '.sc-fFubgz',
            USER: '.sc-idOhPF > .fa-user-circle',
            EMAIL: '.sc-bBXqnf',
            SAIR_CONTA: '.sc-kfzAmx > :nth-child(2) > span',
            SALVAR: '.ant-space > :nth-child(2) > .ant-btn',
            CANCELAR: '.ant-space > :nth-child(1) > .ant-btn',
        },

        DOMINIO: {
            DROPDOWN_CAMPO: '.ant-card-body > .ant-space > :nth-child(1) > .ant-select > .ant-select-selector',
            DROPDOWN: '.ant-select-item-option-content',
            SUBDOMINIO: '.ant-input:eq(0)',
            VALIDAR_BOTAO: '.ant-input-group-addon > .ant-btn',
            TITULO: '.sc-iUuytg',
            LINK: '.ant-space > :nth-child(3) > div > a'
        },

        REDE: {
            EDITAR: ':nth-child(2) > .ant-card-head > .ant-card-head-wrapper > .ant-card-extra > .ant-btn > span',
            CARD: ':nth-child(2) > .ant-card-body',
            VAZIO: '.ant-empty-description',

            LINKS: {
                FACEBOOK: '[href="Facebook"] > .svg-inline--fa',
                TWITTER: '[href="Twitter"] > .svg-inline--fa',
                LINKEDIN: '[href="LinkedIn"] > .svg-inline--fa',
                YOUTUBE: '[href="YouTube"] > .svg-inline--fa',
                INSTAGRAM: '[href="Instagram"] > .svg-inline--fa',
            },

            MODAL: {
                HEADER: '.ant-modal-header',
                BODY: '.ant-modal-body',
                SELECAO_DROPDOWN_CAMPO: '.ant-select-selection-overflow',
                SELECAO_DROPDOWN_OPCAO: '.ant-select-item-option-content',
                FECHAR: '.ant-modal-close-x',

                REMOVER: {
                    PRIMEIRO: ':nth-child(1) > .ant-select-selection-item > .ant-select-selection-item-remove > .anticon > svg',
                    FACEBOOK: ':nth-child(1) > .ant-select-selection-item > .ant-select-selection-item-remove > .anticon > svg',
                    TWITTER: ':nth-child(2) > .ant-select-selection-item > .ant-select-selection-item-remove > .anticon > svg',
                    LINKEDIN: ':nth-child(3) > .ant-select-selection-item > .ant-select-selection-item-remove > .anticon > svg',
                    YOUTUBE: ':nth-child(4) > .ant-select-selection-item > .ant-select-selection-item-remove > .anticon > svg',
                    INSTAGRAM: ':nth-child(5) > .ant-select-selection-item > .ant-select-selection-item-remove > .anticon > svg',
                },
                LINKS:
                {
                    FACEBOOK_CAMPO: ':nth-child(2) > .ant-input-affix-wrapper > .ant-input',
                    TWITTER_CAMPO: ':nth-child(3) > .ant-input-affix-wrapper > .ant-input',
                    LINKEDIN_CAMPO: ':nth-child(4) > .ant-input-affix-wrapper > .ant-input',
                    YOUTUBE_CAMPO: ':nth-child(5) > .ant-input-affix-wrapper > .ant-input',
                    INSTAGRAM_CAMPO: ':nth-child(6) > .ant-input-affix-wrapper > .ant-input',
                }
            },

        },

        CORES: {
            TITULO_CARD: ':nth-child(3) > .ant-card-head > .ant-card-head-wrapper > .ant-card-head-title',

            COR_1: ':nth-child(1) > .sc-laRPJI',
            COR_HEX_1: '#rc-editable-input-1',
            PRIMARIA: '.sc-iBaPrD',


            COR_2: ':nth-child(2) > .sc-laRPJI',
            COR_HEX_2: '#rc-editable-input-2',
            SECUNDARIA: '.sc-bBrOnJ',

            COR_3: ':nth-child(3) > .sc-laRPJI',
            COR_HEX_3: '#rc-editable-input-3',
            TEXTO: ':nth-child(1) > .ant-table-column-sorters-with-tooltip > .ant-table-column-sorters > :nth-child(1)',

            COR_4: ':nth-child(4) > .sc-laRPJI',
            COR_HEX_4: '#rc-editable-input-4',
            LOGO: '.sc-iUuytg',
        },

        LOGO: {
            UPLOAD: 'input[type=file]',
            IMG: '.sc-iktFzd',
        },

        PREVIEW: {

            ICON_FACE: ':nth-child(1) > .sc-cBNfnY',
            ICON_TWI: ':nth-child(2) > .sc-cBNfnY',
            ICON_LIN: ':nth-child(3) > .sc-cBNfnY',
            ICON_YOU: ':nth-child(4) > .sc-cBNfnY',
            ICON_INS: ':nth-child(5) > .sc-cBNfnY',

        },

        CLIENTES: {
            ABAS: {
                CLIENTES: '#rc-tabs-0-tab-1',
                CADASTRAR_CLIENTE: '#rc-tabs-1-tab-2',
                RELATORIO: '#rc-tabs-0-tab-3',
            },

            BOTOES: {
                CADASTRAR:'.ant-space > :nth-child(2) > .ant-btn',
                CANCELAR:'[style="margin-right: 8px;"] > .ant-btn',
                SOLICITAR:'.ant-statistic-content-suffix > .ant-btn',
            },

            CAMPOS: {
                NOME:'#usuarioForm_nome',
                EMAIL:'#usuarioForm_email',
                DROPDOWN_CAMPO: '.ant-card-body > .ant-space > :nth-child(1) > .ant-select > .ant-select-selector',
                APURACAO:'.ant-form-item-control-input-content > .ant-select > .ant-select-selector',
                APURACAO_DROPDOWN:'.ant-select-item-option-content',
                EMPRESAS:':nth-child(4) > .ant-row > .ant-form-item-control > .ant-form-item-control-input > .ant-form-item-control-input-content > .ant-select > .ant-select-selector'
                
            },

            MODAL_SOLICITAR:{
                NUMERO_EMPRESAS:'#solicitar-mais-empresas_qtdEmpresas',
                CANCELAR:'.ant-modal-footer > [ant-click-animating-without-extra-node="false"]',
                SOLICITAR:'.ant-modal-footer > .ant-btn-primary',
            },

            OBRIGATORIO:{
                NOME:':nth-child(1) > .ant-row > .ant-form-item-control > .ant-form-item-explain > div',
                EMAIL:':nth-child(2) > .ant-row > .ant-form-item-control > .ant-form-item-explain > div',
                APURACAO:':nth-child(3) > .ant-row > .ant-form-item-control > .ant-form-item-explain > div',
                EMPRESA:':nth-child(4) > .ant-row > .ant-form-item-control > .ant-form-item-explain > div',
            },

            NUM_CADASTRO:'.ant-statistic-content-value > div'
        },

    },







    MENU_ERECUPERADOR: {

        SIMPLES_NACIONAL: ':nth-child(1) > .react-card-flipper > .react-card-front > .sc-crrsfI > .text-center > .ant-btn',
        LUCRO_REAL: ':nth-child(2) > .react-card-flipper > .react-card-front > .sc-crrsfI > .text-center > .ant-btn',
        GERENCIADOR_PROPOSTAS: ':nth-child(3) > .react-card-flipper > .react-card-front > .sc-crrsfI > .text-center > .ant-btn',
        USER: ':nth-child(8) > .fas',
        EMAIL: '.body__caption___email',
        SAIR_CONTA: '.submenu__profile___footer > .ant-btn',
    },

    MENU_ERECUPERADOR_SIMPLES_NACIONAL: {
        RECUPERACAO_PIS: ':nth-child(1) > .react-card-flipper > .react-card-front > .sc-crrsfI > .text-center > .ant-btn',
        SEGRECACAO_RECEITAS: ':nth-child(2) > .react-card-flipper > .react-card-front > .sc-crrsfI > .text-center > .ant-btn',
        RECUPERACAO_DIFAL: ':nth-child(3) > .react-card-flipper > .react-card-front > .sc-crrsfI > .text-center > .ant-btn'
    },

    ERECUPERADOR_BOTOES: {

        MENU: {
            NOVO: '.sidebar__actions > :nth-child(3)',
            TODAS: '[href="/recuperacao"]',
            LISTAR: '.sidebar__actions > :nth-child(4)',
            CLOSE: '.sidebar__actions > .anticon',
            PRIMEIRO_REGISTRO: ':nth-child(1) > .item__action > .item__action___header',
        },

        CADASTRO: {
            CADASTRAR: '.ant-modal-footer > .ant-btn-primary',
            CANCELAR: '.ant-btn-default',
        },

        PRIMEIRO_CADASTRAR: '.ant-empty-footer > .ant-btn',

        INFORMACOES: {
            DADOS: {
                SALVAR: '.ml-2',
                APAGAR_EMPRESA: '.products__data-company___footer > .ant-btn-danger',
                CONFIRMAR_APAGAR: '.ant-modal-footer > div > .ant-btn-primary',
            }
        },
    },

    ERECUPERADOR_BODY: {
        VAZIO: '.ant-empty-description',
        TELA_PRINCIPAL: '.ant-layout-content',
        EMPRESAS: '.item__action > .item__action___header',
        FISCAL_VAZIO: '.ant-tabs-tabpane-active > .ant-empty',
        TABELA_FISCAL: '.ant-table-row',
        HEADER: '.ant-page-header-heading-title'
    },

    ERECUPERADOR_ABAS: {
        INFORMACOES: '.ant-tabs-nav > :nth-child(1) > :nth-child(1)',
        DOCUMENTOS: '.ant-tabs-nav > :nth-child(1) > :nth-child(2)',
        FISCAL: '.ant-tabs-nav > :nth-child(1) > :nth-child(3)',
        RESULTADOS: '.ant-tabs-nav > :nth-child(1) > :nth-child(4)',

        INFORMACOES_DADOS: ':nth-child(1) > .ant-tabs > .ant-tabs-bar > .ant-tabs-nav-container > .ant-tabs-nav-wrap > .ant-tabs-nav-scroll > .ant-tabs-nav > :nth-child(1) > .ant-tabs-tab-active',
        INFORMACOES_CFOP: ':nth-child(1) > .ant-tabs > .ant-tabs-bar > .ant-tabs-nav-container > .ant-tabs-nav-wrap > .ant-tabs-nav-scroll > .ant-tabs-nav > :nth-child(1) > :nth-child(2)',
        INFORMACOES_CONFIGURACOES: ':nth-child(1) > .ant-tabs > .ant-tabs-bar > .ant-tabs-nav-container > .ant-tabs-nav-wrap > .ant-tabs-nav-scroll > .ant-tabs-nav > :nth-child(1) > :nth-child(3)',

    },

    ERECUPERADOR_CAMPOS: {
        CADASTRO: {
            TXT_FILTRO: '.sidebar__body > .ant-input',
            TXT_NOME: '.row > :nth-child(1) > .ant-input',
            TXT_IE: ':nth-child(3) > .ant-input',
            NUM_CNPJ: ':nth-child(2) > .form-control',
            BOX_UF: '.ant-select-selection',
            DROPDOWN_UF: '.ant-select-dropdown-menu-item',
        },

        INFORMACOES: {
            DADOS: {
                TXT_RAZAO: '#RazaoSocial',
                NUM_CNPJ: '#Cnpj',
                NUM_INSC_ESTADUAL: '#Ie',
                TXT_RUA: '#Rua',
                TXT_BAIRRO: '#Bairro',
                TXT_MUNICIPIO: '#Municipio',
                BOX_UF: '.ant-select-selection',
                DROPDOWN_UF: '.ant-select-dropdown-menu-item',
                NUM_NUMERO: '#Numero',
                TXT_COMPLEMENTO: '#Complemento',
                NUM_CEP: '#Cep',
            },
            CFOP: {
                PRIMEIRA_RECIETA: '[data-row-key] > :eq(2)',
                BOX_PRIMEIRO_ICMS: '.ant-select-selection__rendered > :eq(1)',
                DROP_DOWN: '.ant-select-dropdown-menu',

                TABELA: '.ant-table-thead > :eq(0)',
                COLUMN_CODIGO: '[data-row-key] > :nth-child(1)',
                COLUMN_RECEITA: '[data-row-key] > :nth-child(3)',
                COLUMN_ICMS: '[data-row-key] > :nth-child(4)',


                SORT: {
                    CODIGO: '.ant-table-column-sorters > :eq(0)',
                    RECEITA: '.ant-table-column-sorters > :eq(2)',
                    ICMS: '.ant-table-column-sorters > :eq(4)',
                },


                FILTRO: {
                    ABRIR: '.d-flex > .ant-btn',
                    ABRIR_VAZIO: '.ant-empty-footer > .ant-btn',
                    FECHAR: '.ant-drawer-close > .anticon',
                    TELA_FILTRO: '.ant-drawer-body',
                    NUM_CODIGO: '.ant-form-item-children > .ant-input',
                    BOX_RECEITA: '.ant-form-item-children > .ant-select--custom',
                    DROPDOWN_RECEITA_SIM: '.ant-select-dropdown-menu > :eq(1)',
                    DROPDOWN_RECEITA_NAO: '.ant-select-dropdown-menu > :eq(2)',
                    LIMPAR: '.mr-3',
                    FILTRAR: '.ant-drawer__footer > .ant-btn-primary',
                    TELA_VAZIA: '.ant-tabs-tabpane-active > .ant-empty > .ant-empty-description',

                }

            }
        },

        FISCAL: {
            CODIGO: '.row > :nth-child(1) > .ant-input',
            EAN: '.row > :nth-child(2) > .ant-input',
            NCM: '#Ncm',
            EX: ':nth-child(4) > .form-control',
            DESCRICAO: '.col-md-12 > .ant-input',
        },


    },

    MESSAGE: {
        POPUP: '.ant-message-notice-content',
        TOASTIFY: '.ant-notification-notice-message',
        CLOSE: '.ant-notification-close-x',
        ALERT: '.ant-alert',
        ALERT_CLOSE: '.ant-alert-close-icon > .anticon > svg',
    },

    ICONS: {
        EXCLAMATION: '.fa-exclamation',
        CHECK: '.fa-check'
    }


}





export default locators;