import 'cypress-file-upload';
import loc from '../support/locators'
export { user, base, usuario, subBase }

var usuario = 2
// (0)baseJen  (1)baseDev (2)baseCy

var baseJen = 'qa.squad_c@e-auditoria.com.br'
var baseDev = 'tester.squad_c@e-auditoria.com.br'
var baseCy = 'cypresstest@e-auditoria.com.br'

/*Jen*/ var cnpjsJen = ['51.660.544/0001-10', '64.622.366/0001-85', '66.364.632/0001-42', '85.124.517/0001-45', '51.042.272/0001-95', '69.922.730/0001-46', '67.092.335/0001-58', '55.480.566/0001-50'];
/*Dev*/ var cnpjsDev = ['41.620.433/0001-27', '80.743.848/0001-40', '22.148.289/0001-23', '33.708.803/0001-54', '79.814.424/0001-40', '54.654.129/0001-42', '58.361.426/0001-50', '55.480.566/0001-50'];
/*Cy*/ var cnpjsCy = ['99.034.612/0001-62', '54.569.531/0001-29', '78.209.799/0001-18', '32.090.270/0001-27', '26.845.788/0001-20', '97.819.556/0001-46', '27.651.150/0001-11', '55.480.566/0001-50'];

var Jenkins = [baseJen, cnpjsJen, 'subjen']
var Developer = [baseDev, cnpjsDev, 'subdev']
var Cypresss = [baseCy, cnpjsCy, 'subcy']

var base = [Jenkins, Developer, Cypresss]
var user = base[usuario][0]
var subBase = base[usuario][2]


Cypress.Commands.add('testedeBase', () => {
    console.log(base[usuario])  
})


Cypress.Commands.add('login', () => {

    cy.visit(Cypress.config('baseUrl'), { timeout: 30000 })
    cy.get(loc.LOGIN.USER).type(user)
    cy.get(loc.LOGIN.PASSWORD).type('1q2w3e')
    cy.get(loc.LOGIN.BTN_LOGIN).click()
    cy.visit(Cypress.config('baseUrlMult'), { timeout: 30000 })
    cy.get(loc.EMULTIPLICADOR.BODY.TOPO).should('be.visible')
    cy.get(loc.EMULTIPLICADOR.DOMINIO.SUBDOMINIO, { timeout: 30000 }).clear()
    Cypress.on('uncaught:exception', (err, runnable) => {
        return false
    })
})

Cypress.Commands.add('zerarDados', () => {
    cy.wait(500)
    cy.esvaziarRedes()
    cy.wait(500)
    cy.coresPadroes()
    cy.wait(500)
    cy.resetLogo()
    cy.wait(500)
})

Cypress.Commands.add('clearLogin', () => {
    cy.get(loc.EMULTIPLICADOR.BODY.USER, { timeout: 5000 }).click()

    cy.get(loc.EMULTIPLICADOR.BODY.EMAIL).should('be.visible').then(emailLogado => {

        var baseLogada = emailLogado.text()

        if (baseLogada != user) {
            cy.get(loc.EMULTIPLICADOR.BODY.SAIR_CONTA).click()
            //cy.url().should('eq', Cypress.config('baseUrl'))
            cy.url({ timeout: 30000 }).should('eq', 'https://conta.e-auditoria.com.br/Login')
            cy.login()
        }

        else {
            cy.get(loc.EMULTIPLICADOR.BODY.USER).click()
        }

    })
})

Cypress.Commands.add('sairConta', () => {
    cy.get(loc.EMULTIPLICADOR.BODY.USER).should('be.visible')
    cy.wait(500)
    cy.get(loc.EMULTIPLICADOR.BODY.USER, { timeout: 5000 }).click()
    cy.get(loc.EMULTIPLICADOR.BODY.SAIR_CONTA).click()
    cy.url({ timeout: 30000 }).should('eq', 'https://conta.e-auditoria.com.br/Login')
})

Cypress.Commands.add('removerRede', () => {

    cy.get(loc.EMULTIPLICADOR.REDE.CARD, { timeout: 8000 }).should('be.visible').then(text => {

        var cardText = text.text()
        console.log(cardText)

        if (cardText != 'Nenhuma rede social cadastrada') {

            cy.get(loc.EMULTIPLICADOR.REDE.MODAL.REMOVER.PRIMEIRO, { timeout: 5000 }).click({ force: true })
        }
    })
})

Cypress.Commands.add('esvaziarRedes', () => {
    cy.get(loc.EMULTIPLICADOR.REDE.EDITAR, { timeout: 30000 }).click({ force: true })
    for (let count = 0; count < 11; count++) {
        cy.removerRede()
    }
    cy.get(loc.EMULTIPLICADOR.REDE.CARD, { timeout: 5000 }).should('exist')
    cy.get(loc.EMULTIPLICADOR.REDE.MODAL.FECHAR, { timeout: 30000 }).click()
})

Cypress.Commands.add('coresPadroes', () => {
    cy.get(loc.EMULTIPLICADOR.CORES.TITULO_CARD).click()
    cy.get(loc.EMULTIPLICADOR.CORES.TITULO_CARD).click()
    cy.get(loc.EMULTIPLICADOR.CORES.COR_1).should('be.visible').click({ timeout: 5000 })
    cy.get(loc.EMULTIPLICADOR.CORES.COR_HEX_1, { timeout: 5000 }).should('be.visible').clear().type('#FFFFFF')
    cy.get(loc.EMULTIPLICADOR.CORES.TITULO_CARD).click()
    cy.get(loc.EMULTIPLICADOR.CORES.TITULO_CARD).click()
    cy.get(loc.EMULTIPLICADOR.CORES.PRIMARIA).should('have.css', 'background-color', 'rgb(255, 255, 255)')
    cy.get(loc.EMULTIPLICADOR.CORES.TITULO_CARD).click()
    cy.get(loc.EMULTIPLICADOR.CORES.TITULO_CARD).click()
    cy.get(loc.EMULTIPLICADOR.CORES.COR_2).should('be.visible').click({ timeout: 5000 })
    cy.get(loc.EMULTIPLICADOR.CORES.COR_HEX_2, { timeout: 5000 }).should('be.visible').clear().type('#203456')
    cy.get(loc.EMULTIPLICADOR.CORES.SECUNDARIA).should('have.css', 'background-color', 'rgb(32, 52, 86)')
    cy.get(loc.EMULTIPLICADOR.CORES.TITULO_CARD).click()
    cy.get(loc.EMULTIPLICADOR.CORES.TITULO_CARD).click()
    cy.get(loc.EMULTIPLICADOR.CORES.COR_3).should('be.visible').click({ timeout: 5000 })
    cy.get(loc.EMULTIPLICADOR.CORES.COR_HEX_3, { timeout: 5000 }).should('be.visible').clear().type('#FFFFFF')
    cy.get(loc.EMULTIPLICADOR.CORES.TEXTO).should('have.css', 'color', 'rgb(255, 255, 255)')
    cy.get(loc.EMULTIPLICADOR.CORES.TITULO_CARD).click()
    cy.get(loc.EMULTIPLICADOR.CORES.TITULO_CARD).click()
    cy.get(loc.EMULTIPLICADOR.CORES.COR_4).should('be.visible').click({ timeout: 5000 })
    cy.get(loc.EMULTIPLICADOR.CORES.COR_HEX_4, { timeout: 5000 }).clear().type('#203456')
    cy.get(loc.EMULTIPLICADOR.CORES.LOGO).should('have.css', 'color', 'rgb(32, 52, 86)')

    cy.get(loc.EMULTIPLICADOR.CORES.TITULO_CARD).click()
})

Cypress.Commands.add('uploadLogo', (ArquivoUpload) => {
    cy.get(loc.EMULTIPLICADOR.LOGO.UPLOAD).attachFile(ArquivoUpload)
})

Cypress.Commands.add('confirmaUploadLogo', (ArquivoUpload) => {
    cy.get(loc.EMULTIPLICADOR.LOGO.IMG).invoke('attr', 'src').then((img1) => {

        cy.uploadLogo(ArquivoUpload)

        cy.wait(10);

        cy.get(loc.EMULTIPLICADOR.LOGO.IMG).invoke('attr', 'src').then((img2) => {
            expect(img2).to.not.equal(img1)
        });
    });
})

Cypress.Commands.add('confirmarManterLogo', (ArquivoUpload) => {
    cy.get(loc.EMULTIPLICADOR.LOGO.IMG).invoke('attr', 'src').then((img1) => {

        cy.uploadLogo(ArquivoUpload)

        cy.wait(10);

        cy.get(loc.EMULTIPLICADOR.LOGO.IMG).invoke('attr', 'src').then((img2) => {
            expect(img2).equal(img1)
        });
    });
})

Cypress.Commands.add('resetLogo', () => {
    cy.uploadLogo('emultiplicador.jpg')
})

Cypress.Commands.add('checarPersistencia', () => {

    cy.get('.sc-fFubgz').should('be.visible')

    cy.get(loc.EMULTIPLICADOR.DOMINIO.SUBDOMINIO).clear().type('perisistir')

    cy.get(loc.EMULTIPLICADOR.REDE.EDITAR).click()
    cy.get(loc.EMULTIPLICADOR.REDE.MODAL.LINKS.FACEBOOK_CAMPO).should('exist')
    cy.get(loc.EMULTIPLICADOR.REDE.MODAL.LINKS.INSTAGRAM_CAMPO).should('exist')
    cy.get(loc.EMULTIPLICADOR.REDE.MODAL.LINKS.LINKEDIN_CAMPO).should('exist')
    cy.get(loc.EMULTIPLICADOR.REDE.MODAL.LINKS.TWITTER_CAMPO).should('exist')
    cy.get(loc.EMULTIPLICADOR.REDE.MODAL.LINKS.YOUTUBE_CAMPO).should('exist')
    cy.get(loc.EMULTIPLICADOR.REDE.MODAL.FECHAR).click()

    cy.get(loc.EMULTIPLICADOR.CORES.PRIMARIA).should('have.css', 'background-color', 'rgb(255, 0, 0)')
    cy.get(loc.EMULTIPLICADOR.CORES.SECUNDARIA).should('have.css', 'background-color', 'rgb(0, 45, 255)')
    cy.get(loc.EMULTIPLICADOR.CORES.TEXTO).should('have.css', 'color', 'rgb(233, 255, 0)')
    cy.get(loc.EMULTIPLICADOR.CORES.LOGO).should('have.css', 'color', 'rgb(12, 255, 3)')

    const ArquivoUpload = "logo3.png"
    cy.confirmarManterLogo(ArquivoUpload)

})

//COMANDOS RECUPERADOR

Cypress.Commands.add('loginRec', () => {
    cy.visit(Cypress.config('baseUrl'), { timeout: 30000 })
    cy.get(loc.LOGIN.USER).type(user)
    cy.get(loc.LOGIN.PASSWORD).type('1q2w3e')
    cy.get(loc.LOGIN.BTN_LOGIN).click()
    cy.get('.title', { timeout: 30000 }).should('be.visible').should('contain', 'Todas as ferramentas que você precisa em um único lugar')
    cy.get(loc.MODULES.ERECUPERADOR, { timeout: 30000 }).click()
    cy.clearLoginRec()
    cy.get(loc.MENU_ERECUPERADOR.SIMPLES_NACIONAL, { timeout: 30000 }).click()
    cy.get(loc.MENU_ERECUPERADOR_SIMPLES_NACIONAL.RECUPERACAO_PIS, { timeout: 30000 }).click()
    cy.PimeiroRegistroRec()
    Cypress.on('uncaught:exception', (err, runnable) => {
        return false
    })
})

Cypress.Commands.add('clearLoginRec', () => {
    cy.get(loc.MENU_ERECUPERADOR.USER, { timeout: 5000 }).click()

    cy.get(loc.MENU_ERECUPERADOR.EMAIL).should('be.visible').then(emailLogado => {

        var baseLogada = emailLogado.text()

        if (baseLogada != user) {
            cy.get(loc.MENU_ERECUPERADOR.SAIR_CONTA).click()
            cy.url({ timeout: 30000 }).should('eq', 'https://conta.e-auditoria.com.br/Login')
            cy.loginRec()
        }
    })
})

Cypress.Commands.add('sairRec', () => {
    cy.get(loc.MENU_ERECUPERADOR.USER, { timeout: 5000 }).click()
    cy.get(loc.MENU_ERECUPERADOR.SAIR_CONTA).click()
    cy.url({ timeout: 30000 }).should('eq', 'https://conta.e-auditoria.com.br/Login')

})

Cypress.Commands.add('deleteRec', () => {

    cy.get(loc.ERECUPERADOR_BOTOES.MENU.TODAS).click()
    cy.wait(1000)
    cy.get(loc.ERECUPERADOR_BOTOES.MENU.PRIMEIRO_REGISTRO).click()
    cy.get(loc.ERECUPERADOR_ABAS.INFORMACOES).click({ multiple: true })
    cy.get(loc.ERECUPERADOR_BOTOES.INFORMACOES.DADOS.APAGAR_EMPRESA).click()
    cy.get(loc.ERECUPERADOR_BOTOES.INFORMACOES.DADOS.CONFIRMAR_APAGAR).click()
    cy.get(loc.ERECUPERADOR_BODY.HEADER, { timeout: 30000 }).should('contain', 'APURAÇÃO DE CRÉDITO - PIS/COFINS')
})

Cypress.Commands.add('deleteAllRec', () => {


    cy.get(loc.ERECUPERADOR_BODY.EMPRESAS).then(items => {

        let emp = items.map((index, html) => Cypress.$(html).text()).get()
        console.log(emp.length)
        let length = emp.length

        for (let count = 0; count < length; count++) {
            cy.deleteRec()
        }
    })

})

Cypress.Commands.add('PimeiroRegistroRec', () => {

    cy.get(loc.ERECUPERADOR_BODY.TELA_PRINCIPAL).should('be.visible').then(btn => {

        var Default = btn.text()
        console.log(Default)

        cy.get(loc.ERECUPERADOR_BODY.TELA_PRINCIPAL).wait(500).then(btn2 => {

            var Default2 = btn2.text()

            if (Default == Default2) {
                cy.get(loc.ERECUPERADOR_BOTOES.PRIMEIRO_CADASTRAR).click({ timeout: 10000 })

                cy.get(loc.ERECUPERADOR_CAMPOS.CADASTRO.TXT_NOME).type('EMPRESA PADRAO')
                cy.get(loc.ERECUPERADOR_CAMPOS.CADASTRO.TXT_IE).type('000000000')
                cy.get(loc.ERECUPERADOR_CAMPOS.CADASTRO.NUM_CNPJ).type('25.122.123/0001-26')
                cy.get(loc.ERECUPERADOR_BOTOES.CADASTRO.CADASTRAR).click()
                cy.get(loc.MESSAGE.POPUP, { timeout: 30000 }).should('contain', 'Empresa cadastrada com sucesso')
            }

            else {
                cy.deleteAllRec()

                cy.get(loc.ERECUPERADOR_BOTOES.PRIMEIRO_CADASTRAR).click()

                cy.get(loc.ERECUPERADOR_CAMPOS.CADASTRO.TXT_NOME).type('EMPRESA PADRAO')
                cy.get(loc.ERECUPERADOR_CAMPOS.CADASTRO.TXT_IE).type('000000000')
                cy.get(loc.ERECUPERADOR_CAMPOS.CADASTRO.NUM_CNPJ).type('25.122.123/0001-26')
                cy.get(loc.ERECUPERADOR_BOTOES.CADASTRO.CADASTRAR).click()
                cy.get(loc.MESSAGE.POPUP, { timeout: 30000 }).should('contain', 'Empresa cadastrada com sucesso')
            }
        })
    })
})

Cypress.Commands.add('criarEmpresa', (nomeEmp, cnpjEmp) => {
    cy.get(loc.ERECUPERADOR_BOTOES.MENU.NOVO, { timeout: 30000 }).click()
    cy.get(loc.ERECUPERADOR_CAMPOS.CADASTRO.TXT_NOME).type(nomeEmp)
    cy.get(loc.ERECUPERADOR_CAMPOS.CADASTRO.NUM_CNPJ).type(cnpjEmp)
    cy.get(loc.ERECUPERADOR_BOTOES.CADASTRO.CADASTRAR).click()
})

Cypress.Commands.add('empresasCadastroPadrao', () => {
    cy.criarEmpresa('EMPRESA1', base[usuario][1][0])
    cy.criarEmpresa('EMPRESA2', base[usuario][1][1])
    cy.criarEmpresa('EMPRESA3', base[usuario][1][2])
    cy.criarEmpresa('EMPRESA4', base[usuario][1][3])
    cy.criarEmpresa('EMPRESA5', base[usuario][1][4])
    cy.criarEmpresa('EMPRESA_EXTRA', base[usuario][1][5])
    cy.criarEmpresa('EMPRESA_REGRA_LIMITE', base[usuario][1][6])
    cy.criarEmpresa('EMPRESA_REPETIDA', base[usuario][1][7])
})

















Cypress.Commands.add('SortClasificacoes', (SortType) => {

    cy.get('[data-row-key] > :nth-child(' + SortType + ')').then(items => {

        let SortC = items.map((index, html) => Cypress.$(html).text()).get()
        console.log(SortC)

        cy.get(':nth-child(' + SortType + ') > .ant-table-column-sorters-with-tooltip').click()
        cy.wait(500)

        cy.get('[data-row-key] > :nth-child(' + SortType + ')').then(items => {
            let SortD = items.map((index, html) => Cypress.$(html).text()).get()
            console.log(SortD)

            let compare = JSON.stringify(SortD) >= JSON.stringify(SortC)
            console.log(compare)

            if (compare) {
                cy.get('[data-row-key] > :nth-child(1)').should('contain', '')
            }

            else {
                cy.get('[data-row-key] > :nth-child(1)').should('contain', 'Erro na ordenação')
            }


        })

    })
})



Cypress.Commands.add('delete', () => {

    cy.get(loc.ERECUPERADOR_BOTOES.MENU.TODAS).click()
    cy.wait(1000)
    cy.get(loc.ERECUPERADOR_BOTOES.MENU.PRIMEIRO_REGISTRO).click()
    cy.get(loc.ERECUPERADOR_ABAS.INFORMACOES).click({ multiple: true })
    cy.get(loc.ERECUPERADOR_BOTOES.INFORMACOES.DADOS.APAGAR_EMPRESA).click()
    cy.get(loc.ERECUPERADOR_BOTOES.INFORMACOES.DADOS.CONFIRMAR_APAGAR).click()
    cy.get(loc.ERECUPERADOR_BODY.HEADER, { timeout: 30000 }).should('contain', 'APURAÇÃO DE CRÉDITO - PIS/COFINS')
})
Cypress.Commands.add('deleteAll', () => {

    cy.get(loc.ERECUPERADOR_BODY.EMPRESAS).then(items => {

        let emp = items.map((index, html) => Cypress.$(html).text()).get()
        console.log(emp.length)
        let length = emp.length

        for (let count = 0; count < length; count++) {
            cy.delete()
        }
    })
})





Cypress.Commands.add('SortRecuperacoes', (SortType) => {

    cy.get('.ant-table-header-column > .ant-table-column-sorters :eq(' + SortType + ')').click()
    cy.get('[data-row-key] > :nth-child(' + SortType + ')').then(items => {

        let SortC = items.map((index, html) => Cypress.$(html).text()).get()
        console.log(SortC)
        cy.get('.ant-table-header-column > .ant-table-column-sorters :eq(' + SortType + ')').click()
        cy.get('.ant-table-header-column > .ant-table-column-sorters :eq(' + SortType + ')').click()
        cy.wait(500)

        cy.get('[data-row-key] > :nth-child(' + SortType + ')').then(items => {
            let SortD = items.map((index, html) => Cypress.$(html).text()).get()
            console.log(SortD)

            let compare = JSON.stringify(SortD) >= JSON.stringify(SortC)
            console.log(compare)

            if (compare) {
                cy.get('[data-row-key] > :nth-child(1)').should('contain', '')
            }

            else {
                cy.get('[data-row-key] > :nth-child(1)').should('contain', 'Erro na ordenação')
            }


        })

    })
})

Cypress.Commands.add('SortCFOP', (SortCFOPType) => {

    cy.get('.ant-table-header-column > .ant-table-column-sorters :eq(' + SortCFOPType + ')').click()
    cy.get('[data-row-key] > :nth-child(' + SortCFOPType + ')').then(items => {

        let SortC = items.map((index, html) => Cypress.$(html).text()).get()
        console.log(SortC)
        cy.get('.ant-table-header-column > .ant-table-column-sorters :eq(' + SortCFOPType + ')').click()
        cy.get('.ant-table-header-column > .ant-table-column-sorters :eq(' + SortCFOPType + ')').click()

        cy.get('[data-row-key] > :nth-child(' + SortCFOPType + ')').then(items => {
            let SortD = items.map((index, html) => Cypress.$(html).text()).get()
            console.log(SortD)

            let compare = JSON.stringify(SortD) >= JSON.stringify(SortC)
            console.log(compare)

            if (compare) {
                cy.get('[data-row-key] > :nth-child(1)').should('contain', '')
            }

            else {
                cy.get('[data-row-key] > :nth-child(1)').should('contain', 'Erro na ordenação')
            }


        })

    })
})

// Cypress.Commands.add('SortClasificacoes', (SortType) => {
//     cy.get('[data-row-key] > :nth-child(' + SortType + ')').then(items => {

//         let SortC = items.map((index, html) => Cypress.$(html).text()).get()
//         console.log(SortC)

//         cy.get(':nth-child(' + SortType + ') > .ant-table-header-column > .ant-table-column-sorters').click()
//         cy.wait(500)

//         cy.get('[data-row-key] > :nth-child(' + SortType + ')').then(items => {
//             let SortD = items.map((index, html) => Cypress.$(html).text()).get()
//             console.log(SortD)

//             let compare = JSON.stringify(SortD) >= JSON.stringify(SortC)
//             console.log(compare)

//             if (compare) {
//                 cy.get('[data-row-key] > :nth-child(1)').should('contain', '')
//             }

//             else {
//                 cy.get('[data-row-key] > :nth-child(1)').should('contain', 'Erro na ordenação')
//             }


//         })

//     })
// })

Cypress.Commands.add('SwitchRun', () => {
    for (var switchV = 1; switchV <= 6; switchV++) {
        cy.ConfigSwitch(switchV)
    }
})
Cypress.Commands.add('ConfigSwitch', (switchV) => {
    console.log(switchV)
    cy.get(':nth-child(' + switchV + ') > .ant-switch', { timeout: 40000 }).click()
    cy.get(loc.MESSAGE.TOASTIFY, { timeout: 30000 }).should('contain', 'Configuração alterada com sucesso')
    cy.get(loc.MESSAGE.CLOSE).click()
    cy.get('.ant-list-items > :nth-child(' + switchV + ')').then(items => {

        let Switch0 = items.map((index, html) => Cypress.$(html).text()).get()
        console.log(Switch0)

        cy.get(':nth-child(' + switchV + ') > .ant-switch', { timeout: 40000 }).click()
        cy.get(loc.MESSAGE.TOASTIFY, { timeout: 40000 }).should('contain', 'Configuração alterada com sucesso')
        cy.get(loc.MESSAGE.CLOSE).click()

        cy.get('.ant-list-items > :nth-child(' + switchV + ')').then(items => {
            let Switch1 = items.map((index, html) => Cypress.$(html).text()).get()
            console.log(Switch1)

            let compare = JSON.stringify(Switch1) != JSON.stringify(Switch0)
            console.log(compare)

            if (compare) {
                cy.get('.ant-list-items > :nth-child(' + switchV + ')').should('contain', '')
            }

            else {
                cy.get('.ant-list-items > :nth-child(' + switchV + ')').should('contain', 'Erro na ordenação')
            }

        })

    })
})

