/// <reference types="cypress"/>
import loc from '../support/locators'
import 'cypress-file-upload';
import {subBase} from '../support/commands'

describe('1_E-MULTIPLICADOR_CRIACAO_SISTEMA', () => {
    before(() => {
        cy.login()
        cy.clearLogin()
        cy.zerarDados()

    });
    
    describe('Testes de Domínio', () => {

        it.skip('Teste não deve validar domínio em branco e impedir de salvar', () => {

            //cy.get(loc.EMULTIPLICADOR.DOMINIO.TITULO).should('contain', 'Domínio')
            //cy.get(loc.EMULTIPLICADOR.DOMINIO.SUBDOMINIO).should('not.exist')
            //cy.get(loc.EMULTIPLICADOR.DOMINIO.VALIDAR_BOTAO).should('not.exist')
            cy.get(loc.MESSAGE.ALERT).should('not.exist')
            cy.get(loc.EMULTIPLICADOR.BODY.SALVAR).contains('Salvar').click()
            //cy.get(loc.MESSAGE.ALERT).scrollIntoView().should('contain', 'Dominio: Selecione um domínio válido')
            cy.get(loc.MESSAGE.ALERT).should('contain', 'Subdominio: Este campo é de preenchimento obrigatório')
            cy.get(loc.MESSAGE.ALERT_CLOSE).click()
            cy.get(loc.EMULTIPLICADOR.DOMINIO.DROPDOWN_CAMPO).scrollIntoView().should('be.visible').click()
            cy.get(loc.EMULTIPLICADOR.DOMINIO.DROPDOWN).contains('audisimples.com.br').click()
            cy.get(loc.EMULTIPLICADOR.DOMINIO.VALIDAR_BOTAO, { timeout: 5000 }).click()
            cy.get(loc.EMULTIPLICADOR.BODY.SALVAR).contains('Salvar').click()
            cy.get(loc.MESSAGE.ALERT).should('not.contain', 'Dominio: Selecione um domínio válido')
            cy.get(loc.MESSAGE.ALERT).should('contain', 'Subdominio: Este campo é de preenchimento obrigatório')
            cy.get(loc.MESSAGE.ALERT_CLOSE).click()
        });

        it.only('Teste não deve validar subdomínio em branco e impedir de salvar', () => {
            cy.get(loc.EMULTIPLICADOR.DOMINIO.DROPDOWN_CAMPO).scrollIntoView().should('be.visible').click()
            cy.get(loc.EMULTIPLICADOR.DOMINIO.DROPDOWN).contains('audisimples.com.br').click()
            cy.get(loc.EMULTIPLICADOR.DOMINIO.VALIDAR_BOTAO, { timeout: 5000 }).click()
            cy.get(loc.MESSAGE.POPUP, { timeout: 5000 }).should('contain', 'O subdomínio deve conter 3 ou mais caracteres')
            cy.get(loc.ICONS.EXCLAMATION, { timeout: 5000 }).should('not.exist')
            cy.get(loc.ICONS.CHECK, { timeout: 5000 }).should('not.exist')
        });

        it('Teste não deve validar subdomínio sem começar com letra, conter símbolos e acentos com exceção de "-"', () => {

            cy.get(':nth-child(1) > .ant-card-head > .ant-card-head-wrapper > .ant-card-head-title').click()
            cy.get(loc.EMULTIPLICADOR.DOMINIO.DROPDOWN_CAMPO).click()
            cy.get(loc.EMULTIPLICADOR.DOMINIO.DROPDOWN).contains('audisimples.com.br').click({ timeout: 5000 })


            cy.get(loc.EMULTIPLICADOR.DOMINIO.SUBDOMINIO).clear().type('aa')
            cy.get(loc.EMULTIPLICADOR.DOMINIO.VALIDAR_BOTAO).click({ timeout: 5000 })
            cy.get(loc.ICONS.EXCLAMATION).should('exist')
            cy.get(loc.MESSAGE.POPUP, { timeout: 5000 }).should('contain', 'O subdomínio deve conter 3 ou mais caracteres')
            cy.get(loc.EMULTIPLICADOR.BODY.SALVAR).contains('Salvar').click()
            cy.get(loc.MESSAGE.ALERT).should('contain', 'Subdominio: Deve conter pelo menos 3 caracteres')
            cy.get(loc.MESSAGE.ALERT_CLOSE).click()

            cy.get(loc.EMULTIPLICADOR.DOMINIO.SUBDOMINIO).clear().type('!@#$%¨&*()')
            cy.get(loc.EMULTIPLICADOR.DOMINIO.VALIDAR_BOTAO, { timeout: 5000 }).click()
            cy.get(loc.ICONS.EXCLAMATION).should('exist')
            cy.get(loc.MESSAGE.POPUP, { timeout: 5000 }).should('contain', 'O subdomínio deve começar com letra e conter somente letras, números ou "-"')

            cy.get(loc.EMULTIPLICADOR.DOMINIO.SUBDOMINIO).clear().type('123123123123')
            cy.get(loc.EMULTIPLICADOR.DOMINIO.VALIDAR_BOTAO).click()
            cy.get(loc.ICONS.EXCLAMATION).should('exist')
            cy.get(loc.MESSAGE.POPUP, { timeout: 5000 }).should('contain', 'O subdomínio deve começar com letra e conter somente letras, números ou "-"')

            cy.get(loc.EMULTIPLICADOR.DOMINIO.SUBDOMINIO).clear().type('óóóóó')
            cy.get(loc.EMULTIPLICADOR.DOMINIO.VALIDAR_BOTAO).click()
            cy.get(loc.ICONS.EXCLAMATION).should('exist')
            cy.get(loc.MESSAGE.POPUP, { timeout: 5000 }).should('contain', 'O subdomínio deve começar com letra e conter somente letras, números ou "-"')
            cy.get(loc.EMULTIPLICADOR.DOMINIO.SUBDOMINIO).clear()

            // cy.get(loc.EMULTIPLICADOR.DOMINIO.SUBDOMINIO).clear().type('MAIUSCULAS')
            // cy.get(loc.EMULTIPLICADOR.DOMINIO.VALIDAR_BOTAO).click()
            // cy.get(loc.ICONS.EXCLAMATION).should('exist')
            // cy.get(loc.MESSAGE.POPUP, { timeout: 5000 }).should('contain', 'O subdomínio deve começar com letra e conter somente letras, números ou "-"')


        });

        it('Teste deve validar domínio com subdomínio com letra e/ou números e "-" (após letras)', () => {
            cy.get(loc.EMULTIPLICADOR.DOMINIO.SUBDOMINIO).clear().type(subBase)
            cy.get(loc.EMULTIPLICADOR.DOMINIO.VALIDAR_BOTAO, { timeout: 5000 }).click()
            cy.get(loc.ICONS.CHECK).should('exist')
            cy.get(loc.EMULTIPLICADOR.DOMINIO.LINK).should('contain', 'https://audisimples.com.br/' + subBase)


            cy.get(loc.EMULTIPLICADOR.DOMINIO.SUBDOMINIO).clear().type('teste1')
            cy.get(loc.EMULTIPLICADOR.DOMINIO.VALIDAR_BOTAO, { timeout: 5000 }).click()
            cy.get(loc.ICONS.CHECK).should('exist')
            cy.get(loc.EMULTIPLICADOR.DOMINIO.LINK).should('contain', 'https://audisimples.com.br/teste1')

            cy.get(loc.EMULTIPLICADOR.DOMINIO.SUBDOMINIO).clear().type('teste-00')
            cy.get(loc.EMULTIPLICADOR.DOMINIO.VALIDAR_BOTAO, { timeout: 5000 }).click()
            cy.get(loc.ICONS.CHECK).should('exist')
            cy.get(loc.EMULTIPLICADOR.DOMINIO.LINK).should('contain', 'https://audisimples.com.br/teste-00')
        });

        it('Teste não deve conseguir validar domínio com subdomínio maior que 50 caracteres', () => {
            cy.get(loc.EMULTIPLICADOR.DOMINIO.DROPDOWN_CAMPO).click()
            cy.get(loc.EMULTIPLICADOR.DOMINIO.DROPDOWN).contains('audisimples.com.br').click()
            cy.get(loc.EMULTIPLICADOR.DOMINIO.SUBDOMINIO).clear().type('aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaab')
            cy.get(loc.EMULTIPLICADOR.DOMINIO.VALIDAR_BOTAO, { timeout: 50000 }).click()
            cy.get(loc.ICONS.CHECK).should('exist')
            cy.get(loc.MESSAGE.POPUP, { timeout: 5000 }).should('contain', 'O subdomínio deve conter no maximo 50 caracteres')
            cy.get(loc.EMULTIPLICADOR.DOMINIO.LINK).should('contain', 'https://audisimples.com.br/aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa')
            cy.get(loc.EMULTIPLICADOR.DOMINIO.LINK).should('not.contain', 'https://audisimples.com.br/aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaab')
        });

        it('Teste deve mudar nome no Preview do Sistema ao selecionar domínio', () => {
            cy.get(loc.EMULTIPLICADOR.DOMINIO.DROPDOWN_CAMPO).click()
            cy.get(loc.EMULTIPLICADOR.DOMINIO.DROPDOWN).contains('certificasimples.com.br').click()
            cy.get(loc.EMULTIPLICADOR.DOMINIO.TITULO).should('contain', 'Certifica Simples')
            
        });

    });

    describe('Testes de Rede Social', () => {
        it('Teste deve checar campos vazios', () => {

            cy.get(loc.EMULTIPLICADOR.REDE.VAZIO).should('contain', 'Nenhuma rede social cadastrada')
            cy.get(loc.EMULTIPLICADOR.REDE.EDITAR).click({ force: true })
            cy.get(loc.EMULTIPLICADOR.REDE.MODAL.HEADER, { timeout: 10000 }).should('be.visible')
            cy.get(loc.EMULTIPLICADOR.REDE.MODAL.REMOVER.FACEBOOK).should('not.exist')
            cy.get(loc.EMULTIPLICADOR.REDE.MODAL.REMOVER.INSTAGRAM).should('not.exist')
            cy.get(loc.EMULTIPLICADOR.REDE.MODAL.REMOVER.LINKEDIN).should('not.exist')
            cy.get(loc.EMULTIPLICADOR.REDE.MODAL.REMOVER.TWITTER).should('not.exist')
            cy.get(loc.EMULTIPLICADOR.REDE.MODAL.REMOVER.YOUTUBE).should('not.exist')
            cy.get(loc.EMULTIPLICADOR.REDE.MODAL.FECHAR).click()
        });

        it('Teste conseguir criar campos e remove-los', () => {
            cy.esvaziarRedes()
            cy.get(loc.EMULTIPLICADOR.REDE.EDITAR, { timeout: 30000 }).click({ force: true })
            cy.get(loc.EMULTIPLICADOR.REDE.MODAL.HEADER, { timeout: 10000 }).should('be.visible')
            cy.get(loc.EMULTIPLICADOR.REDE.MODAL.SELECAO_DROPDOWN_CAMPO, { timeout: 10000 }).should('be.visible').click({ force: true })
            cy.get(loc.EMULTIPLICADOR.REDE.MODAL.SELECAO_DROPDOWN_OPCAO, { timeout: 10000 }).contains('Facebook').click()
            cy.get(loc.EMULTIPLICADOR.REDE.MODAL.LINKS.FACEBOOK_CAMPO).should('exist')
            cy.get(loc.EMULTIPLICADOR.REDE.MODAL.REMOVER.FACEBOOK).should('exist')
            cy.get(loc.EMULTIPLICADOR.REDE.MODAL.SELECAO_DROPDOWN_OPCAO).contains('Twitter').click({ force: true })
            cy.get(loc.EMULTIPLICADOR.REDE.MODAL.LINKS.TWITTER_CAMPO).should('exist')
            cy.get(loc.EMULTIPLICADOR.REDE.MODAL.REMOVER.TWITTER).should('exist')
            cy.get(loc.EMULTIPLICADOR.REDE.MODAL.SELECAO_DROPDOWN_OPCAO).contains('LinkedIn').click({ force: true })
            cy.get(loc.EMULTIPLICADOR.REDE.MODAL.LINKS.LINKEDIN_CAMPO).should('exist')
            cy.get(loc.EMULTIPLICADOR.REDE.MODAL.REMOVER.LINKEDIN).should('exist')
            cy.get(loc.EMULTIPLICADOR.REDE.MODAL.SELECAO_DROPDOWN_OPCAO).contains('YouTube').click({ force: true })
            cy.get(loc.EMULTIPLICADOR.REDE.MODAL.LINKS.YOUTUBE_CAMPO).should('exist')
            cy.get(loc.EMULTIPLICADOR.REDE.MODAL.REMOVER.YOUTUBE).should('exist')
            cy.get(loc.EMULTIPLICADOR.REDE.MODAL.SELECAO_DROPDOWN_OPCAO).contains('Instagram').click({ force: true })
            cy.get(loc.EMULTIPLICADOR.REDE.MODAL.LINKS.INSTAGRAM_CAMPO).should('exist')
            cy.get(loc.EMULTIPLICADOR.REDE.MODAL.REMOVER.INSTAGRAM).should('exist')
            cy.get(loc.EMULTIPLICADOR.REDE.MODAL.REMOVER.PRIMEIRO, { timeout: 50000 }).should('be.visible').click({ force: true })
            cy.get(loc.EMULTIPLICADOR.REDE.MODAL.REMOVER.PRIMEIRO).should('exist').click({ force: true })
            cy.get(loc.EMULTIPLICADOR.REDE.MODAL.REMOVER.PRIMEIRO).should('exist').click({ force: true })
            cy.get(loc.EMULTIPLICADOR.REDE.MODAL.REMOVER.PRIMEIRO).should('exist').click({ force: true })
            cy.get(loc.EMULTIPLICADOR.REDE.MODAL.REMOVER.PRIMEIRO).should('exist').click({ force: true })

            cy.get(loc.EMULTIPLICADOR.REDE.MODAL.FECHAR).click()
        })

        it.skip('Teste não deve salvar links inválidos', () => {
            cy.esvaziarRedes()
            var FaceLink = 'teste'
            var TwiLink = 'teste'
            var LinLink = 'teste'
            var YouLink = 'teste'
            var InsLink = 'teste'


            cy.get(loc.EMULTIPLICADOR.REDE.EDITAR).click({ force: true })
            cy.get(loc.EMULTIPLICADOR.REDE.MODAL.HEADER, { timeout: 10000 }).should('be.visible')
            cy.get(loc.EMULTIPLICADOR.REDE.MODAL.SELECAO_DROPDOWN_CAMPO).should('be.visible').click()

            cy.get(loc.EMULTIPLICADOR.REDE.MODAL.SELECAO_DROPDOWN_OPCAO).contains('Facebook').click()
            cy.get(loc.EMULTIPLICADOR.REDE.MODAL.SELECAO_DROPDOWN_OPCAO).contains('Twitter').click()
            cy.get(loc.EMULTIPLICADOR.REDE.MODAL.SELECAO_DROPDOWN_OPCAO).contains('LinkedIn').click()
            cy.get(loc.EMULTIPLICADOR.REDE.MODAL.SELECAO_DROPDOWN_OPCAO).contains('YouTube').click()
            cy.get(loc.EMULTIPLICADOR.REDE.MODAL.SELECAO_DROPDOWN_OPCAO).contains('Instagram').click()

            cy.get(loc.EMULTIPLICADOR.REDE.MODAL.LINKS.FACEBOOK_CAMPO).type(FaceLink, { force: true })
            cy.get(loc.EMULTIPLICADOR.REDE.MODAL.LINKS.TWITTER_CAMPO).type(TwiLink)
            cy.get(loc.MESSAGE.POPUP, { timeout: 5000 }).should('contain', "O link 'facebook' não está válido")
            cy.get(loc.EMULTIPLICADOR.REDE.MODAL.LINKS.LINKEDIN_CAMPO).type(LinLink)
            cy.get(loc.MESSAGE.POPUP, { timeout: 5000 }).should('contain', "O link 'twitter' não está válido")
            cy.get(loc.EMULTIPLICADOR.REDE.MODAL.LINKS.YOUTUBE_CAMPO).type(YouLink)
            cy.get(loc.MESSAGE.POPUP, { timeout: 5000 }).should('contain', "O link 'linkedin' não está válido")
            cy.get(loc.EMULTIPLICADOR.REDE.MODAL.LINKS.INSTAGRAM_CAMPO).type(InsLink)
            cy.get(loc.MESSAGE.POPUP, { timeout: 5000 }).should('contain', "O link 'youtube' não está válido")
            cy.get(loc.EMULTIPLICADOR.REDE.MODAL.FECHAR).click()
            cy.get(loc.MESSAGE.POPUP, { timeout: 5000 }).should('contain', "O link 'instagram' não está válido")

            cy.get(loc.EMULTIPLICADOR.REDE.LINKS.FACEBOOK).should('not.exist')
            cy.get(loc.EMULTIPLICADOR.REDE.LINKS.TWITTER).should('not.exist')
            cy.get(loc.EMULTIPLICADOR.REDE.LINKS.LINKEDIN).should('not.exist')
            cy.get(loc.EMULTIPLICADOR.REDE.LINKS.YOUTUBE).should('not.exist')
            cy.get(loc.EMULTIPLICADOR.REDE.LINKS.INSTAGRAM).should('not.exist')

            cy.get(loc.EMULTIPLICADOR.PREVIEW.ICON_FACE).should('not.exist')
            cy.get(loc.EMULTIPLICADOR.PREVIEW.ICON_TWI).should('not.exist')
            cy.get(loc.EMULTIPLICADOR.PREVIEW.ICON_LIN).should('not.exist')
            cy.get(loc.EMULTIPLICADOR.PREVIEW.ICON_YOU).should('not.exist')
            cy.get(loc.EMULTIPLICADOR.PREVIEW.ICON_INS).should('not.exist')

        })

        it('Teste deve criar campos, preenche-los remover e não encontrar botões no Preview', () => {

            cy.esvaziarRedes()
            var FaceLink = 'https://www.facebook.com/'
            var TwiLink = 'https://twitter.com/'
            var LinLink = 'https://br.linkedin.com/'
            var YouLink = 'https://www.youtube.com/'
            var InsLink = 'https://www.instagram.com/?hl=pt-br'


            cy.get(loc.EMULTIPLICADOR.REDE.EDITAR).click({ force: true })
            cy.get(loc.EMULTIPLICADOR.REDE.MODAL.HEADER, { timeout: 10000 }).should('be.visible').click()
            cy.get(loc.EMULTIPLICADOR.REDE.MODAL.SELECAO_DROPDOWN_CAMPO, { timeout: 10000 }).should('be.visible').click()

            cy.get(loc.EMULTIPLICADOR.REDE.MODAL.SELECAO_DROPDOWN_OPCAO, { timeout: 10000 }).contains('Facebook').click()
            cy.get(loc.EMULTIPLICADOR.REDE.MODAL.SELECAO_DROPDOWN_OPCAO).contains('Twitter').click()
            cy.get(loc.EMULTIPLICADOR.REDE.MODAL.SELECAO_DROPDOWN_OPCAO).contains('LinkedIn').click()
            cy.get(loc.EMULTIPLICADOR.REDE.MODAL.SELECAO_DROPDOWN_OPCAO).contains('YouTube').click()
            cy.get(loc.EMULTIPLICADOR.REDE.MODAL.SELECAO_DROPDOWN_OPCAO).contains('Instagram').click()

            cy.get(loc.EMULTIPLICADOR.REDE.MODAL.LINKS.FACEBOOK_CAMPO).type(FaceLink, { force: true })
            cy.get(loc.EMULTIPLICADOR.REDE.MODAL.LINKS.TWITTER_CAMPO).type(TwiLink)
            cy.get(loc.EMULTIPLICADOR.REDE.MODAL.LINKS.LINKEDIN_CAMPO).type(LinLink)
            cy.get(loc.EMULTIPLICADOR.REDE.MODAL.LINKS.YOUTUBE_CAMPO).type(YouLink)
            cy.get(loc.EMULTIPLICADOR.REDE.MODAL.LINKS.INSTAGRAM_CAMPO).type(InsLink)
            cy.get(loc.EMULTIPLICADOR.REDE.MODAL.FECHAR).click()

            cy.esvaziarRedes()

            cy.get(loc.EMULTIPLICADOR.REDE.LINKS.FACEBOOK).should('not.exist')
            cy.get(loc.EMULTIPLICADOR.REDE.LINKS.TWITTER).should('not.exist')
            cy.get(loc.EMULTIPLICADOR.REDE.LINKS.LINKEDIN).should('not.exist')
            cy.get(loc.EMULTIPLICADOR.REDE.LINKS.YOUTUBE).should('not.exist')
            cy.get(loc.EMULTIPLICADOR.REDE.LINKS.INSTAGRAM).should('not.exist')

            cy.get(loc.EMULTIPLICADOR.PREVIEW.ICON_FACE).should('not.exist')
            cy.get(loc.EMULTIPLICADOR.PREVIEW.ICON_TWI).should('not.exist')
            cy.get(loc.EMULTIPLICADOR.PREVIEW.ICON_LIN).should('not.exist')
            cy.get(loc.EMULTIPLICADOR.PREVIEW.ICON_YOU).should('not.exist')
            cy.get(loc.EMULTIPLICADOR.PREVIEW.ICON_INS).should('not.exist')

        })

        it('Testes devem conseguir editar botões/campos de rede social e salvar no Preview', () => {
            cy.esvaziarRedes()
            var FaceLink = 'https://www.facebook.com/'
            var TwiLink = 'https://twitter.com/'
            var LinLink = 'https://br.linkedin.com/'
            var YouLink = 'https://www.youtube.com/'
            var InsLink = 'https://www.instagram.com/?hl=pt-br'


            cy.get(loc.EMULTIPLICADOR.REDE.EDITAR).click({ timeout: 5000 })
            cy.get(loc.EMULTIPLICADOR.REDE.MODAL.HEADER, { timeout: 10000 }).should('be.visible')
            cy.get(loc.EMULTIPLICADOR.REDE.MODAL.SELECAO_DROPDOWN_CAMPO).should('be.visible').click()

            cy.get(loc.EMULTIPLICADOR.REDE.MODAL.SELECAO_DROPDOWN_OPCAO).contains('Facebook').click()
            cy.get(loc.EMULTIPLICADOR.REDE.MODAL.SELECAO_DROPDOWN_OPCAO).contains('Twitter').click()
            cy.get(loc.EMULTIPLICADOR.REDE.MODAL.SELECAO_DROPDOWN_OPCAO).contains('LinkedIn').click()
            cy.get(loc.EMULTIPLICADOR.REDE.MODAL.SELECAO_DROPDOWN_OPCAO).contains('YouTube').click()
            cy.get(loc.EMULTIPLICADOR.REDE.MODAL.SELECAO_DROPDOWN_OPCAO).contains('Instagram').click()

            cy.get(loc.EMULTIPLICADOR.REDE.MODAL.LINKS.FACEBOOK_CAMPO).type(FaceLink, { force: true })
            cy.get(loc.EMULTIPLICADOR.REDE.MODAL.LINKS.TWITTER_CAMPO).type(TwiLink)
            cy.get(loc.EMULTIPLICADOR.REDE.MODAL.LINKS.LINKEDIN_CAMPO).type(LinLink)
            cy.get(loc.EMULTIPLICADOR.REDE.MODAL.LINKS.YOUTUBE_CAMPO).type(YouLink)
            cy.get(loc.EMULTIPLICADOR.REDE.MODAL.LINKS.INSTAGRAM_CAMPO).type(InsLink)
            cy.get(loc.EMULTIPLICADOR.REDE.MODAL.FECHAR).click()

            cy.get(loc.EMULTIPLICADOR.PREVIEW.ICON_FACE).should('have.attr', 'href', FaceLink)
            cy.get(loc.EMULTIPLICADOR.PREVIEW.ICON_TWI).should('have.attr', 'href', TwiLink)
            cy.get(loc.EMULTIPLICADOR.PREVIEW.ICON_LIN).should('have.attr', 'href', LinLink)
            cy.get(loc.EMULTIPLICADOR.PREVIEW.ICON_YOU).should('have.attr', 'href', YouLink)
            cy.get(loc.EMULTIPLICADOR.PREVIEW.ICON_INS).should('have.attr', 'href', InsLink)


            var FaceLink = 'https://www.facebook.com/teste'
            var TwiLink = 'https://twitter.com/teste'
            var LinLink = 'https://br.linkedin.com/teste'
            var YouLink = 'https://www.youtube.com/teste'
            var InsLink = 'https://www.instagram.com/?hl=pt-brteste'


            cy.get(loc.EMULTIPLICADOR.REDE.EDITAR).click({ timeout: 5000 })
            cy.get(loc.EMULTIPLICADOR.REDE.MODAL.HEADER, { timeout: 10000 }).should('be.visible')
            cy.get(loc.EMULTIPLICADOR.REDE.MODAL.SELECAO_DROPDOWN_CAMPO).should('be.visible').click()

            cy.get(loc.EMULTIPLICADOR.REDE.MODAL.LINKS.INSTAGRAM_CAMPO, { force: true }).click()
            cy.get(loc.EMULTIPLICADOR.REDE.MODAL.LINKS.FACEBOOK_CAMPO).clear().type(FaceLink)
            cy.get(loc.EMULTIPLICADOR.REDE.MODAL.LINKS.TWITTER_CAMPO).clear().type(TwiLink)
            cy.get(loc.EMULTIPLICADOR.REDE.MODAL.LINKS.LINKEDIN_CAMPO).clear().type(LinLink)
            cy.get(loc.EMULTIPLICADOR.REDE.MODAL.LINKS.YOUTUBE_CAMPO).clear().type(YouLink)
            cy.get(loc.EMULTIPLICADOR.REDE.MODAL.LINKS.INSTAGRAM_CAMPO).clear().type(InsLink)
            cy.get(loc.EMULTIPLICADOR.REDE.MODAL.FECHAR).click()

            cy.get(loc.EMULTIPLICADOR.PREVIEW.ICON_FACE).should('have.attr', 'href', FaceLink)
            cy.get(loc.EMULTIPLICADOR.PREVIEW.ICON_TWI).should('have.attr', 'href', TwiLink)
            cy.get(loc.EMULTIPLICADOR.PREVIEW.ICON_LIN).should('have.attr', 'href', LinLink)
            cy.get(loc.EMULTIPLICADOR.PREVIEW.ICON_YOU).should('have.attr', 'href', YouLink)
            cy.get(loc.EMULTIPLICADOR.PREVIEW.ICON_INS).should('have.attr', 'href', InsLink)

        })

    });

    describe('Testes de Preview', () => {
        it.skip('Teste deve validar a ordenação apresentada no preview do site', () => {
            var SortType = 1;
            cy.get(':nth-child(' + SortType + ') > .ant-table-column-sorters-with-tooltip').click()
            cy.SortClasificacoes(SortType)

            var SortType = 2;

            cy.SortClasificacoes(SortType)

            var SortType = 3;

            cy.SortClasificacoes(SortType)

            var SortType = 4;

            cy.SortClasificacoes(SortType)
        });

        it('Teste deve confirmar a mudança de cores no preview ', () => {

            cy.get(loc.EMULTIPLICADOR.CORES.COR_1).should('be.visible').click({ timeout: 3000 })
            cy.get(loc.EMULTIPLICADOR.CORES.COR_HEX_1, { timeout: 3000 }).should('be.visible').clear().type('#04C38E')
            cy.get(loc.EMULTIPLICADOR.CORES.PRIMARIA).should('have.css', 'background-color', 'rgb(4, 195, 142)')
            cy.get(loc.EMULTIPLICADOR.CORES.TITULO_CARD).click()
            cy.get(loc.EMULTIPLICADOR.CORES.TITULO_CARD).click()
            cy.get(loc.EMULTIPLICADOR.CORES.COR_2).should('be.visible').click({ timeout: 3000 })
            cy.get(loc.EMULTIPLICADOR.CORES.COR_HEX_2, { timeout: 3000 }).should('be.visible').clear().type('#162332')
            cy.get(loc.EMULTIPLICADOR.CORES.SECUNDARIA).should('have.css', 'background-color', 'rgb(22, 35, 50)')
            cy.get(loc.EMULTIPLICADOR.CORES.TITULO_CARD).click()
            cy.get(loc.EMULTIPLICADOR.CORES.TITULO_CARD).click()
            cy.get(loc.EMULTIPLICADOR.CORES.COR_3).should('be.visible').click({ timeout: 3000 })
            cy.get(loc.EMULTIPLICADOR.CORES.COR_HEX_3, { timeout: 3000 }).should('be.visible').clear().type('#04C38E')
            cy.get(loc.EMULTIPLICADOR.CORES.TEXTO).should('have.css', 'color', 'rgb(4, 195, 142)')
            cy.get(loc.EMULTIPLICADOR.CORES.TITULO_CARD).click()
            cy.get(loc.EMULTIPLICADOR.CORES.TITULO_CARD).click()
            cy.get(loc.EMULTIPLICADOR.CORES.COR_4).should('be.visible').click({ timeout: 3000 })
            cy.get(loc.EMULTIPLICADOR.CORES.COR_HEX_4, { timeout: 3000 }).should('be.visible').clear().type('#FFFFFF')
            cy.get(loc.EMULTIPLICADOR.CORES.LOGO).should('have.css', 'color', 'rgb(255, 255, 255)')

            cy.get(loc.EMULTIPLICADOR.CORES.TITULO_CARD).click()
        });
    });

    describe('Testes de upload da logo', () => {
        it('Teste não deve fazer upload de um formato de arquivo diferente ', () => {
            const ArquivoUpload = "0.txt"
            cy.confirmarManterLogo(ArquivoUpload)
            cy.get(loc.MESSAGE.POPUP, { timeout: 5000 }).should('contain', ArquivoUpload + " não é um arquivo válido")

        })

        it('Testes de upload fazer o upload de uma nova imagem em JPG e PNG e confirmar sua mudança', () => {
            var ArquivoUpload = "logo.jpg"
            cy.confirmaUploadLogo(ArquivoUpload)
            ArquivoUpload = "logo.png"
            cy.confirmaUploadLogo(ArquivoUpload)

        });

        it.skip('Teste de proporção de imagem', () => {
            //LIMITE DE TAMANHO???
        });

    });

    describe('Teste de persistencia de dados', () => {

        it('Teste recuperar dados ao cancelar alterações', () => {
            cy.get(loc.EMULTIPLICADOR.DOMINIO.SUBDOMINIO).clear().type(subBase + 'cancelar')
            cy.get(loc.EMULTIPLICADOR.DOMINIO.VALIDAR_BOTAO).click()
            cy.get(loc.EMULTIPLICADOR.DOMINIO.LINK).should('contain', 'https://certificasimples.com.br/' + subBase + 'cancelar')

            cy.esvaziarRedes()
            cy.get(loc.EMULTIPLICADOR.REDE.EDITAR, { timeout: 30000 }).click({ force: true })
            cy.get(loc.EMULTIPLICADOR.REDE.MODAL.SELECAO_DROPDOWN_CAMPO, { timeout: 10000 }).should('be.visible').click({ force: true })
            cy.get(loc.EMULTIPLICADOR.REDE.MODAL.SELECAO_DROPDOWN_OPCAO, { timeout: 10000 }).contains('Facebook').click()
            cy.get(loc.EMULTIPLICADOR.REDE.MODAL.SELECAO_DROPDOWN_OPCAO).contains('Twitter').click({ force: true })
            cy.get(loc.EMULTIPLICADOR.REDE.MODAL.SELECAO_DROPDOWN_OPCAO).contains('LinkedIn').click({ force: true })
            cy.get(loc.EMULTIPLICADOR.REDE.MODAL.SELECAO_DROPDOWN_OPCAO).contains('YouTube').click({ force: true })
            cy.get(loc.EMULTIPLICADOR.REDE.MODAL.SELECAO_DROPDOWN_OPCAO).contains('Instagram').click({ force: true })
            cy.get(loc.EMULTIPLICADOR.REDE.MODAL.FECHAR).click()

            cy.get(loc.EMULTIPLICADOR.CORES.COR_1).should('be.visible').click({ timeout: 3000 })
            cy.get(loc.EMULTIPLICADOR.CORES.COR_HEX_1, { timeout: 3000 }).clear().type('#FF0000')

            cy.get(loc.EMULTIPLICADOR.CORES.TITULO_CARD).click()
            cy.get(loc.EMULTIPLICADOR.CORES.TITULO_CARD).click()
            cy.get(loc.EMULTIPLICADOR.CORES.COR_2).should('be.visible').click({ timeout: 3000 })
            cy.get(loc.EMULTIPLICADOR.CORES.COR_HEX_2, { timeout: 3000 }).clear().type('#002DFF')

            cy.get(loc.EMULTIPLICADOR.CORES.TITULO_CARD).click()
            cy.get(loc.EMULTIPLICADOR.CORES.TITULO_CARD).click()
            cy.get(loc.EMULTIPLICADOR.CORES.COR_3).should('be.visible').click({ timeout: 3000 })
            cy.get(loc.EMULTIPLICADOR.CORES.COR_HEX_3, { timeout: 3000 }).clear().type('#E9FF00')

            cy.get(loc.EMULTIPLICADOR.CORES.TITULO_CARD).should('be.visible').click()
            cy.get(loc.EMULTIPLICADOR.CORES.TITULO_CARD).should('be.visible').click()
            cy.get(loc.EMULTIPLICADOR.CORES.COR_4).should('be.visible').click({ timeout: 3000 })
            cy.get(loc.EMULTIPLICADOR.CORES.COR_HEX_4, { timeout: 3000 }).clear().type('#0CFF03')

            var ArquivoUpload = 'logo3.png'
            cy.uploadLogo(ArquivoUpload)

            cy.get(loc.EMULTIPLICADOR.BODY.SALVAR).click()

            cy.zerarDados()
            
            cy.get(loc.EMULTIPLICADOR.BODY.CANCELAR).click()
            cy.checarPersistencia()
        });

        it('Teste deve manter dados ao salvar', () => {

            cy.checarPersistencia()

            cy.visit('https://emultiplicador.e-auditoria.com.br/home', { timeout: 30000 })

            cy.checarPersistencia()
        });
    });
})