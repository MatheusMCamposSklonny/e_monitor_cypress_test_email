// /// <reference types="cypress"/>
// import loc from '../support/locators'
// import 'cypress-file-upload';
// import { user, base, usuario, subBase } from '../support/commands'

// describe('2_E-MULTIPLICADOR_E-RECUPERADOR_REGISTROS', () => {
//     before(() => {
//         // cy.loginRec()
//         // cy.empresasCadastroPadrao()
//         // cy.sairRec()

//         cy.login()
//         cy.clearLogin()
//         cy.get(loc.EMULTIPLICADOR.BODY.CANCELAR).click()
//         // cy.get(loc.EMULTIPLICADOR.CLIENTES.BOTOES.CANCELAR).click()
//         console.log(base[usuario][2])
//     });

//     describe('Testes de Gerenciamento de Clientes', () => {
//         it('Teste deve não criar cadastro de cliente vazio', () => {

//             cy.get(loc.EMULTIPLICADOR.CLIENTES.CAMPOS.NOME).clear()
//             cy.get(loc.EMULTIPLICADOR.CLIENTES.CAMPOS.EMAIL).clear()
//             cy.get(loc.EMULTIPLICADOR.CLIENTES.BOTOES.CADASTRAR).click()
//             cy.get(loc.EMULTIPLICADOR.CLIENTES.OBRIGATORIO.NOME).should('contain', 'Por favor, informe o Nome')
//             cy.get(loc.EMULTIPLICADOR.CLIENTES.OBRIGATORIO.EMAIL).should('contain', 'Por favor, informe o E-mail')
//             cy.get(loc.EMULTIPLICADOR.CLIENTES.OBRIGATORIO.APURACAO).should('contain', 'Por favor, selecione o Módulo')
//             cy.get(loc.EMULTIPLICADOR.CLIENTES.CAMPOS.EMPRESAS).should('not.exist')
//             cy.get(loc.EMULTIPLICADOR.CLIENTES.CAMPOS.APURACAO, { timeout: 10000 }).should('be.visible').click()
//             cy.get(loc.EMULTIPLICADOR.CLIENTES.CAMPOS.APURACAO_DROPDOWN, { timeout: 10000 }).contains('Análise por CFOP (Módulo Recuperação de Pis/Cofins para Empresas do Simples)').click()
//             cy.get(loc.EMULTIPLICADOR.CLIENTES.BOTOES.CADASTRAR).click()
//             cy.get(loc.EMULTIPLICADOR.CLIENTES.OBRIGATORIO.EMPRESA).should('contain', 'Por favor, selecione a(s) Empresa(s)')

//         })

//         it('Teste deve testar validação de e-mail', () => {

//             cy.get(loc.EMULTIPLICADOR.CLIENTES.CAMPOS.EMAIL).type('validar')
//             cy.get(loc.EMULTIPLICADOR.CLIENTES.CAMPOS.EMAIL).clear().type('1231231231')
//             cy.get(loc.EMULTIPLICADOR.CLIENTES.CAMPOS.EMAIL).clear().type('!#$%¨&*()')
//             cy.get(loc.EMULTIPLICADOR.CLIENTES.CAMPOS.EMAIL).clear().type('@@@@@@@@@@')
//             cy.get(loc.EMULTIPLICADOR.CLIENTES.CAMPOS.EMAIL).clear().type('..........')
//             cy.get(loc.EMULTIPLICADOR.CLIENTES.CAMPOS.EMAIL).clear().type('àéîõü')
//             cy.get(loc.EMULTIPLICADOR.CLIENTES.CAMPOS.EMAIL).clear().type(user)
//         })

//         it.only('Teste deve criar registro de cnpj já cadstrado em outra base', () => {


//             cy.wait(500)


//             cy.get(loc.EMULTIPLICADOR.CLIENTES.CAMPOS.NOME).type('a')
//             cy.get('.ant-select-selection-overflow').type('a')
//             // cy.get(loc.EMULTIPLICADOR.CLIENTES.CAMPOS.EMPRESAS,{timeout:3000}).should('not.be.disabled').click().type('EMPRESA_REGRA_LIMITE')
//             // cy.get('.ant-select-dropdown-placement-bottomLeft').contains('EMPRESA_REGRA_LIMITE').click()




//         })

//     });
// })